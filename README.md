## Development server

1. Ejecutar `npm install` para instalar dependencias del proyecto.
2. Ejecutar `npm start` para ejecutar el servidor dev.
3. Se debe iniciar el navegador deshabilitando la seguridad. En caso de CHROME se debe ejecutar (esto para la ejecución del servicio deshabilitando CORS), lo cual tendrá que abrir el navegador:
    "[PATH]\chrome.exe" --disable-web-security --disable-gpu --user-data-dir=~/chromeTemp

## Build webpack
1. Ejecutar `npm run build:webpack`

## Architecture
La aplicación fue construida con Angular 8 haciendo uso de rxjs y Redux como patrón de diseño para el control global de los estados.
Para el tema de REDUX, el módulo comic tiene una carpeta llamada "store" la cual aloja todos los archivos necesarios para funcionar.
El archivo de configuración customizada de webpack es webpack.config.js el cual se ubica en la raiz del proyecto.