import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Comic } from '../modules/comic/entities/comic';

@Injectable()
export class ComicServices {
  constructor(private http: HttpClient) { }

  public getComic(): Observable<Comic> {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE,OPTIONS,HEAD'});
    return this.http.get<Comic>('https://xkcd.com/info.0.json', {
      headers
   });
  }
}
