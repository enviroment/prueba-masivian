import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'comic',
    loadChildren: () => import('./modules/comic/comic.module').then(m => m.ComicModule)
  },
  {
    path: '',
    redirectTo: 'comic',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
