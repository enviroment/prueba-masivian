import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { ComicServices } from 'src/app/services/comic.service';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { fetchComicAction, fetchComicSuccessAction } from '../actions/comic.actions';
import { switchMap, filter, mergeMap } from 'rxjs/operators';

@Injectable()
export class ComicEffects {
  constructor(private actions$: Actions,
              private service: ComicServices) { }

  FetchComic$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchComicAction),
      switchMap(() =>
        this.service.getComic()
          .pipe(filter(data => !!data))
      ),
      mergeMap(response => [
        fetchComicSuccessAction({ response })
      ])
    ));
}
