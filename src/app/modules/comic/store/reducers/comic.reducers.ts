import { createReducer, on } from '@ngrx/store';
import { rateComicAction, fetchComicSuccessAction } from '../actions/comic.actions';
import { Comic } from '../../entities/comic';

export const rateComicInitial: { stars: number } = {} as { stars: number };
export const comicInitial: Comic = {} as Comic;

export const rateComicReducer = createReducer(
  rateComicInitial,
  on(rateComicAction,
    (state, { response: { stars } }) =>
    ({ stars } ))
);

export const fetchComicReducer = createReducer(
  comicInitial,
  on(fetchComicSuccessAction,
    (state, { response }) =>
    ({ ...response }))
);
