import { rateComicReducer as rateComic,
  fetchComicReducer as fetchComic } from './comic.reducers';
import { combineReducers } from '@ngrx/store';

export const ComicReducer = combineReducers({
  rateComic,
  fetchComic
});
