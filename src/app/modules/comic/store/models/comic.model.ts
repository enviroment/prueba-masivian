import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { ComicModuleState } from '../states/comic.states';
import { Comic } from '../../entities/comic';
import { fetchComicAction, rateComicAction } from '../actions/comic.actions';
import { fetchComicSelector } from '../selectors/comic.selectors';

@Injectable()
export class ComicModel {

  constructor(protected store: Store<ComicModuleState>) { }

  public getComic$: Observable<Comic> = this.store.pipe(select(fetchComicSelector));
  public getRating$: Observable<{ stars: number }> = this.store.select(store => store.rateComic);

  public fetchComic(): void {
    this.store.dispatch(fetchComicAction());
  }

  public rateComic(stars: number): void {
    this.store.dispatch(rateComicAction({ response: { stars } }));
  }
}
