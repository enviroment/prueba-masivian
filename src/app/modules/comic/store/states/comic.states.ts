import { Comic } from '../../entities/comic';
import { from } from 'src/app/share/utils';

export type ComicModuleState = Readonly<{
  rateComic: { stars: number };
  fetchComic: Comic;
}>;

export const INITIAL_COMIC_MODULE_STATE: ComicModuleState = {
  rateComic: from({}),
  fetchComic: from({})
};
