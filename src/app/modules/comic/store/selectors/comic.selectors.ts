import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ComicModuleState } from '../states/comic.states';

export const comicSelector = createFeatureSelector<ComicModuleState>('comics');

export const fetchComicSelector = createSelector(comicSelector, (state: ComicModuleState) => state.fetchComic);
