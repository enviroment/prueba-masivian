import { createAction, props } from '@ngrx/store';
import { type } from 'src/app/share/utils';
import { Comic } from '../../entities/comic';

export const rateComicAction = createAction(type('[Comic] Rate action'),
  props<{ response: { stars: number } }>());

export const fetchComicAction = createAction(type('[Comic] Fetch comic action'));

export const fetchComicSuccessAction = createAction(type('[Comic] Fetch comic action success'),
  props<{ response: Comic }>());
