export interface Rating {
  id: string;
  value: string;
  title: string;
  class: string;
}
