import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Rating } from '../../entities/rating';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.sass']
})
export class RatingComponent implements OnChanges {
  @Input() ratings: Array<Rating> = [];
  @Output() rate: EventEmitter<string> = new EventEmitter();

  public rating: string;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ratings) {
      this.rate.emit(this.rating);
    }
  }
}
