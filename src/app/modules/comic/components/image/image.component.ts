import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.sass']
})
export class ImageComponent {
  @Input() defaultImage = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/343086/h8fnwL1.png';
  @Input() image: string;
  @Input() alt: string;
}
