import { Component } from '@angular/core';
import { ComicModel } from './store/models/comic.model';
import { Observable } from 'rxjs';
import { Comic } from './entities/comic';
import { filter, tap } from 'rxjs/operators';
import { Rating } from './entities/rating';

@Component({
  selector: 'app-comic',
  templateUrl: './comic.component.html',
  styleUrls: ['./comic.component.sass']
})
export class ComicComponent {
  public rate: string;

  constructor(private model: ComicModel) { }

  get comic$(): Observable<Comic> {
    return this.model.getComic$.pipe(
      tap(data => console.log('Comic response:', data)),
      filter(data => !!data)
    );
  }

  get ratings(): Array<Rating> {
    return [
      {
        id: 'star10',
        value: '10',
        title: '10 stars',
        class: 'full'
      },
      {
        id: 'star9half',
        value: '9 and a half',
        title: '9.5 stars',
        class: 'half'
      },
      {
        id: 'star9',
        value: '9',
        title: '9 stars',
        class: 'full'
      },
      {
        id: 'star8half',
        value: '8 and a half',
        title: '8.5 stars',
        class: 'half'
      },
      {
        id: 'star8',
        value: '8',
        title: '8 stars',
        class: 'full'
      },
      {
        id: 'star7half',
        value: '7 and a half',
        title: '7.5 stars',
        class: 'half'
      },
      {
        id: 'star7',
        value: '7',
        title: '7 stars',
        class: 'full'
      },
      {
        id: 'star6half',
        value: '6 and a half',
        title: '6.5 stars',
        class: 'half'
      },
      {
        id: 'star6',
        value: '6',
        title: '6 star',
        class: 'full'
      },
      {
        id: 'star5half',
        value: '5 and a half',
        title: '5.5 stars',
        class: 'half'
      },
      {
        id: 'star5',
        value: '5',
        title: '5 stars',
        class: 'full'
      },
      {
        id: 'star4half',
        value: '4 and a half',
        title: '4.5 stars',
        class: 'half'
      },
      {
        id: 'star4',
        value: '4',
        title: '4 stars',
        class: 'full'
      },
      {
        id: 'star3half',
        value: '3 and a half',
        title: '3.5 stars',
        class: 'half'
      },
      {
        id: 'star3',
        value: '3',
        title: '3 stars',
        class: 'full'
      },
      {
        id: 'star2half',
        value: '2 and a half',
        title: '2.5 stars',
        class: 'half'
      },
      {
        id: 'star2',
        value: '2',
        title: '2 stars',
        class: 'full'
      },
      {
        id: 'star1half',
        value: '1 and a half',
        title: '1.5 stars',
        class: 'half'
      },
      {
        id: 'star1',
        value: '1',
        title: '1 stars',
        class: 'full'
      },
      {
        id: 'starhalf',
        value: 'half',
        title: '0.5 stars',
        class: 'half'
      }
    ];
  }

  public rateComic(rate: string): void {
    this.rate = rate;
  }
}
