import { NgModule, InjectionToken } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComicComponent } from './comic.component';
import { ImageComponent } from './components/image/image.component';
import { RatingComponent } from './components/rating/rating.component';
import { ComicRoutingModule } from './comic-routing.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { ComicServices } from 'src/app/services/comic.service';
import { ComicModel } from './store/models/comic.model';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { ComicModuleState } from './store/states/comic.states';
import { ComicReducer } from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { ComicEffects } from './store/effects/comic.effects';
import { FormsModule } from '@angular/forms';

export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<ComicModuleState>>('Root Reducer');

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComicRoutingModule,
    LazyLoadImageModule,
    StoreModule.forFeature('comics', REDUCER_TOKEN),
    EffectsModule.forFeature(
      [
        ComicEffects
      ]
    )
  ],
  declarations: [
    ComicComponent,
    ImageComponent,
    RatingComponent
  ],
  providers: [
    ComicServices,
    ComicModel,
    {
      provide: REDUCER_TOKEN,
      useValue: ComicReducer
    }
  ]
})
export class ComicModule {
  constructor(model: ComicModel) {
    model.fetchComic();
  }
}
