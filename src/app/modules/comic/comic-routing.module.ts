import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ComicComponent } from './comic.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ComicComponent
      }
    ])
  ],
  exports: [ RouterModule ]
})
export class ComicRoutingModule {

}
